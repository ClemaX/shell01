#! /bin/sh

find . -name '*.sh' \( -type f -or -type d \) -exec basename {} .sh \;
