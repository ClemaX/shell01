#! /bin/sh

cat /etc/passwd | sed '/#/d' | tail -n +2 | awk 'NR % 2' | cut -d ':' -f1 | rev | sort -r | sed -n "$FT_LINE1 , $FT_LINE2 p" | tr '\n' ',' | sed 's/,/, /g' | sed 's/, $/./' | tr -d '\n'
